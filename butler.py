#! /usr/bin/python3

import daemon
import time

from src.terminal import pyxhook


class KeyboardState(object):
    SCAN_CODE_Q = 24
    SCAN_CODE_LEFT_ALT = 64

    scan_codes = {}

    @classmethod
    def key_down(cls, scan_code):
        cls.scan_codes[scan_code] = True

    @classmethod
    def key_up(cls, scan_code):
        cls.scan_codes.pop(scan_code)

    @classmethod
    def key_state(cls, scan_code):
        return cls.scan_codes.get(scan_code, False)


def key_down_event(event):
    KeyboardState.key_down(event.ScanCode)

    if event.ScanCode == KeyboardState.SCAN_CODE_Q and KeyboardState.key_state(KeyboardState.SCAN_CODE_LEFT_ALT):
        print('FIRE!')


def key_up_event(event):
    KeyboardState.key_up(event.ScanCode)


def main():
    hookman = pyxhook.HookManager()

    hookman.KeyDown = key_down_event
    hookman.KeyUp = key_up_event
    hookman.HookKeyboard()

    hookman.start()
    try:
        while True:
            time.sleep(1)
    except:
        pass
    hookman.cancel()


if __name__ == '__main__':
    main()
    # with daemon.DaemonContext():
    #     main()
