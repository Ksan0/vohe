import subprocess
import argparse
import os


class ArgumentParserException(Exception):
    pass


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(formatter_class=argparse.RawTextHelpFormatter)

    def _print_message(self, message, file=None):
        pass

    def exit(self, status=0, message=None):
        raise ArgumentParserException()


def run_program(path, *args):
    full_args = [path]
    full_args.extend(args)
    subprocess.Popen(executable=path, args=full_args, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def run_browser(ctx, url):
    run_program(ctx.get('browser'), url)
    system('wmctrl -a %s' % ctx.get('browser-wmctrl'))


# noinspection PyProtectedMember
def shell(ctx, command):
    ctx._exit(command)


def system(command):
    os.system(command)
