class StatisticElement(object):
    def __init__(self, mode, date, counter):
        self.mode = mode
        self.date = date
        self.counter = counter

    def __repr__(self):
        return '{} | {} | {}'.format(self.date, self.mode, self.counter)
