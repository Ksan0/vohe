class Context(object):
    def __init__(self, module):
        self.__module = module

    def _exit(self, command):
        self.__module.exit(command)

    def get(self, key):
        return self.__module.db.get_context_value(key)

    def set(self, key, value):
        self.__module.db.set_context_value(key, value)

    def get_path(self):
        return self.__module.db.get_context_path()

    def set_path(self, path):
        self.__module.db.set_context_path(path)
