import os
import sqlite3
import itertools

from datetime import datetime

from src.db.command import Command
from src.db.history import HistoryElement
from src.db.statistic import StatisticElement
from src.db.word import Word


# noinspection SqlNoDataSourceInspection,SqlResolve
class DB(object):
    def __init__(self, path):
        self.__path = path

        need_create_tables = not os.path.isfile(path)
        self.__conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES)
        self.__c = self.__conn.cursor()

        if need_create_tables:
            self.__create_tables()

    def close(self):
        if self.is_open():
            self.__c.close()
            self.__conn.commit()
            self.__conn.close()
            self.__conn = None

    def is_open(self):
        return self.__conn is not None

    def add_primary_words(self, words):
        if not words:
            return

        params = ','.join(('(NULL, ?)' for w in words))
        query = 'INSERT OR IGNORE INTO words VALUES {}'.format(params)
        self.__c.execute(query, [w.word for w in words])

        for w in words:
            w.id = self.__find_word_id_primary(w.word)

    def get_all_words(self):
        self.__c.execute('SELECT id, word FROM words')
        words = [Word(el[0], el[1], []) for el in self.__c.fetchall()]
        return words

    def find_word(self, word):
        word_id = self.__find_word_id_primary(word)
        if word_id is None:
            word_id = self.__find_word_id_synonym(word)

        links = self.__get_word_links(word_id)
        return Word(word_id, word, links)

    def get_linked_words(self, word):
        left = self.__get_linked_words(word, True)
        right = self.__get_linked_words(word, False)
        return left, right

    def add_synonym(self, word, synonym):
        word_id = self.__find_word_id_primary(word)
        if word_id is None:
            raise KeyError('Word "{}" not found'.format(word))

        try:
            self.__c.execute('INSERT INTO synonyms VALUES (?, ?)', (word_id, synonym))
        except sqlite3.IntegrityError:
            raise ValueError('Duplicate synonym "{}"'.format(synonym))

    def remove_synonym(self, synonym):
        self.__c.execute('DELETE FROM synonyms WHERE synonym=?', (synonym,))
        if self.__c.rowcount == 0:
            raise ValueError('Synonym "{}" not found'.format(synonym))

    def add_command(self, command):
        command_key = self.__get_command_key(command.words)

        try:
            command.id = self.__c.execute('INSERT INTO commands VALUES (NULL, ?)', (command_key,)).lastrowid
        except sqlite3.IntegrityError:
            raise KeyError('Command "{}" already exist'.format(command))

        params = ','.join(('(?, ?)' for x in command.words))
        query = 'INSERT OR IGNORE INTO words_to_commands VALUES {}'.format(params)
        params = []
        for el in ((w.id, command.id) for w in command.words):
            params.extend(el)
        self.__c.execute(query, params)

        left, right = itertools.tee(command.words)
        next(right, None)
        for l, r in zip(left, right):
            l.links.append(r.id)
            try:
                self.__c.execute('INSERT INTO words_links VALUES(?, ?)', (l.id, r.id))
            except sqlite3.IntegrityError:
                pass

    def remove_command(self, command):
        self.__c.execute('DELETE FROM commands WHERE id=?', (command.id,))
        self.__c.execute('DELETE FROM words_to_commands WHERE command_id=?', (command.id,))

    def get_command(self, words, params):
        command_key = self.__get_command_key(words)
        obj = self.__c.execute('SELECT id FROM commands WHERE words=?', (command_key,)).fetchone()
        return Command(obj and obj[0], words, params)

    def get_all_commands(self):
        self.__c.execute('SELECT id FROM commands')
        commands_ids = [el[0] for el in self.__c.fetchall()]
        commands = self.__get_commands_words(commands_ids)
        return commands

    def has_command(self, words, params):
        return bool(self.get_command(words, params))

    def find_commands_with_words(self, words):
        commands_ids = None
        for w in words:
            self.__c.execute('SELECT command_id FROM words_to_commands WHERE word_id=?', (w.id,))
            ids = set(el[0] for el in self.__c.fetchall())
            commands_ids = ids if commands_ids is None else commands_ids.intersection(ids)

        return self.__get_commands_words(commands_ids)

    def add_history(self, mode, words):
        self.__c.execute('INSERT INTO history VALUES (?, ?, ?)', (mode, datetime.now(), ' '.join(words)))

    def get_history(self, limit):
        self.__c.execute('SELECT mode, date, input FROM history LIMIT ?', (limit,))
        return [HistoryElement(el[0], el[1], el[2]) for el in self.__c.fetchall()]

    def add_statistic(self, mode):
        date = datetime.now().strftime('%Y-%m-%d')
        self.__c.execute('UPDATE statistic SET counter=counter+1 WHERE mode=? AND date=?', (mode, date))
        if self.__c.rowcount == 0:
            self.__c.execute('INSERT INTO statistic VALUES (?, ?, ?)', (mode, date, 1))

    def get_statistic(self, limit):
        self.__c.execute('SELECT mode, date, counter FROM statistic LIMIT ?', (limit,))
        return [StatisticElement(el[0], el[1], el[2]) for el in self.__c.fetchall()]

    def set_context_value(self, key, value):
        path = self.get_context_path()
        if path != '':
            key = '{}.{}'.format(path, key)
        self.__set_context_value(key, value)

    def get_context_value(self, key):
        path = self.get_context_path()
        path = path.split('.') if path != '' else []
        key = key.split('.') if key != '' else []

        for i in range(len(path)):
            full_key = list(path)
            full_key.extend(key)
            value = self.__get_context_value('.'.join(full_key))
            if value is not None:
                return value
            path.pop()

        return self.__get_context_value('.'.join(key))

    def set_context_path(self, path):
        os.environ['RCG_CTX'] = path
        # self.__set_context_value('_.path', path)

    def get_context_path(self):
        return os.environ.get('RCG_CTX', '')
        # obj = self.__get_context_value('_.path')
        # return obj if obj is not None else ''

    def __set_context_value(self, key, value):
        self.__c.execute('UPDATE context SET value=? WHERE key=?', (value, key))
        if self.__c.rowcount == 0:
            self.__c.execute('INSERT INTO context VALUES (?, ?)', (key, value))

    def __get_context_value(self, key):
        obj = self.__c.execute('SELECT value FROM context WHERE key=?', (key,)).fetchone()
        return obj and obj[0]

    def __get_linked_words(self, word, left):
        if left:
            self.__c.execute('SELECT right_word_id FROM words_links WHERE left_word_id=?', (word.id,))
        else:
            self.__c.execute('SELECT left_word_id FROM words_links WHERE right_word_id=?', (word.id,))

        words_ids = [el[0] for el in self.__c.fetchall()]
        words = self.__execute_in('SELECT id, word FROM words WHERE id=?', words_ids)
        return [Word(el[0], el[1], []) for el in words]

    def __find_word_id_primary(self, word):
        obj = self.__c.execute('SELECT id FROM words WHERE word=?', (word,)).fetchone()
        return obj and obj[0]

    def __find_word_id_synonym(self, word):
        obj = self.__c.execute('SELECT word_id FROM synonyms WHERE synonym=?', (word,)).fetchone()
        return obj and obj[0]

    def __get_word_links(self, word_id):
        if word_id is not None:
            self.__c.execute('SELECT right_word_id FROM words_links WHERE left_word_id=?', (word_id,))
            return [e[0] for e in self.__c.fetchall()]
        else:
            return []

    def __get_commands_words(self, commands_ids):
        commands = []
        for cid in commands_ids:
            key = self.__c.execute('SELECT words FROM commands WHERE id=?', (cid,)).fetchone()[0]
            ids = [int(x) for x in key.split('_')]
            ws = self.__execute_in('SELECT id, word FROM words WHERE id=?', ids)
            ws = [Word(el[0], el[1], []) for el in ws]
            commands.append(Command(cid, ws, []))
        return commands

    def __execute_in(self, query, args):
        res = (self.__c.execute(query, (x,)).fetchone() for x in args)
        return (x for x in res if x is not None)

    def __get_command_key(self, words):
        return '_'.join([str(w.id) for w in words])

    def __create_tables(self):
        self.__c.execute(
            'CREATE TABLE words ('
            'id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'word TEXT NOT NULL UNIQUE'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE synonyms ('
            'word_id INTEGER NOT NULL,'
            'synonym TEXT NOT NULL UNIQUE,'
            'FOREIGN KEY (word_id) REFERENCES words(id)'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE words_links ('
            'left_word_id INTEGER,'
            'right_word_id INTEGER,'
            'FOREIGN KEY (left_word_id) REFERENCES words(id),'
            'FOREIGN KEY (right_word_id) REFERENCES words(id),'
            'UNIQUE (left_word_id, right_word_id)'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE commands ('
            'id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'words TEXT NOT NULL UNIQUE'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE words_to_commands ('
            'word_id INTEGER NOT NULL,'
            'command_id INTEGER NOT NULL,'
            'FOREIGN KEY (word_id) REFERENCES words(id),'
            'FOREIGN KEY (command_id) REFERENCES commands(id),'
            'UNIQUE (word_id, command_id)'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE context ('
            'key TEXT PRIMARY KEY,'
            'value TEXT'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE history ('
            'mode TEXT,'
            'date TIMESTAMP,'
            'input TEXT'
            ')'
        )
        self.__c.execute(
            'CREATE TABLE statistic ('
            'mode TEXT,'
            'date TEXT,'
            'counter INTEGER'
            ')'
        )

        self.__set_context_value('_.self-terminal-editor', 'nano')
        self.__set_context_value('browser', 'google-chrome')
        self.__set_context_value('browser-wmctrl', 'chrome')

        self.__conn.commit()
