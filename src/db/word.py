class Word(object):
    def __init__(self, word_id, word, links):
        self.id = word_id
        self.word = word
        self.links = list(links)

    def __bool__(self):
        return self.id is not None

    def __repr__(self):
        return '{}@{}'.format(self.id, self.word)
