class HistoryElement(object):
    def __init__(self, mode, date, row):
        self.mode = mode
        self.date = date
        self.row = row

    def __repr__(self):
        return '{} | {} | {}'.format(self.date.strftime('%Y-%m-%d %H:%M:%S'), self.mode, self.row)
