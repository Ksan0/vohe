class Command(object):
    def __init__(self, command_id, words, params):
        self.id = command_id
        self.words = words
        self.params = params

    def __bool__(self):
        return self.id is not None

    def __repr__(self):
        kw = ' '.join([w.word for w in self.words])
        params = ' '.join([w.word for w in self.params])
        return '[{}@{}]'.format(kw, params)
