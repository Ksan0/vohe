import os
import traceback
from importlib.machinery import SourceFileLoader

from src.conf.conf import Conf
from src.db.context import Context
from src.modules.module import Module


class ExplainModule(Module):
    def __init__(self, db, command, script_path):
        super().__init__(db)
        self.command = command
        self.script_path = script_path

    def exec(self):
        if not os.path.isfile(self.script_path):
            print('Error: Script does not exist "{}"'.format(self.command))
            self.exit()

        context = Context(self)
        try:
            module = SourceFileLoader('', self.script_path).load_module()
            module.explain(context, [w.word for w in self.command.params])
        except BaseException as e:
            traceback.print_exception(type(e), e, None)
        finally:
            self.exit()
