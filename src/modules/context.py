from src.modules.module import Module


class ContextModule(Module):
    def __init__(self, db, args):
        super().__init__(db)
        self.args = args

    def exec(self):
        if len(self.args.words) <= 1:
            path = self.args.words[0] if self.args.words else ''
            self.db.set_context_path(path)
        else:
            print('Error: Excepted one path argument')

        self.exit()
