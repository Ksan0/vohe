from src.modules.module import Module


class FindWordsModule(Module):
    def __init__(self, db, args, words):
        super().__init__(db)
        self.args = args
        self.words = words

    def exec(self):
        if len(self.words) <= 1:
            if self.words:
                if self.words[0]:
                    self.__print_linked_words(self.words[0])
                else:
                    print('Error: Unknown word')
            else:
                self.__print_all_words()
        else:
            print('Error: One word expected')

        self.exit()

    def __print_all_words(self):
        words = self.db.get_all_words()
        for w in words:
            print(w.word)

        if len(words) == 0:
            print('Error: No words found')

    def __print_linked_words(self, word):
        left, right = self.db.get_linked_words(word)

        for w in left:
            print('{} -> {}'.format(word.word, w.word))
        for w in right:
            print('{} -> {}'.format(w.word, word.word))

        if len(left) + len(right) == 0:
            print('Error: No links found')
