import argparse

import sys

from src.modules.context import ContextModule
from src.modules.module import Module
from src.modules.synonym import SynonymModule
from src.modules.words import WordsModule


class InputArgs(object):
    MODE_EXECUTE = 'execute'
    MODE_EXPLAIN = 'explain'
    MODE_ADD = 'add'
    MODE_EDIT = 'edit'
    MODE_DELETE = 'delete'

    MODE_FIND_WORDS = 'find-words'
    MODE_FIND_COMMANDS = 'find-commands'

    MODE_SYNONYM_ADD = 'synonym-add'
    MODE_SYNONYM_DELETE = 'synonym-delete'

    MODE_CONTEXT_CHANGE = 'context-change'

    MODE_HISTORY = 'history'
    MODE_STATISTIC = 'statistic'

    def __init__(self, mode, words):
        self.mode = mode
        self.words = words

    def is_mode_execute(self):
        return self.mode == self.MODE_EXECUTE

    def is_mode_explain(self):
        return self.mode == self.MODE_EXPLAIN

    def is_mode_add(self):
        return self.mode == self.MODE_ADD

    def is_mode_edit(self):
        return self.mode == self.MODE_EDIT

    def is_mode_delete(self):
        return self.mode == self.MODE_DELETE

    def is_mode_find_words(self):
        return self.mode == self.MODE_FIND_WORDS

    def is_mode_find_commands(self):
        return self.mode == self.MODE_FIND_COMMANDS

    def is_mode_synonym_add(self):
        return self.mode == self.MODE_SYNONYM_ADD

    def is_mode_synonym_delete(self):
        return self.mode == self.MODE_SYNONYM_DELETE

    def is_mode_context_change(self):
        return self.mode == self.MODE_CONTEXT_CHANGE

    def is_mode_history(self):
        return self.mode == self.MODE_HISTORY

    def is_mode_statistic(self):
        return self.mode == self.MODE_STATISTIC


class ArgumentParserException(Exception):
    pass


class ArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        if message:
            self._print_message(message, sys.stderr)
        raise ArgumentParserException()


class InputModule(Module):
    def exec(self):
        args = self.parse_args()
        if args is not None:
            if args.is_mode_history():
                self.__print_history()
            elif args.is_mode_statistic():
                self.__print_statistic()
            elif args.is_mode_synonym_add() or args.is_mode_synonym_delete():
                return SynonymModule(self.db, args)
            elif args.is_mode_context_change():
                return ContextModule(self.db, args)
            else:
                self.db.add_history(args.mode, args.words)
                self.db.add_statistic(args.mode)
                return WordsModule(self.db, args)

        self.exit()

    def parse_args(self):
        parser = ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

        parser.add_argument(
            '--mode',
            choices=(
                InputArgs.MODE_ADD,
                InputArgs.MODE_EDIT,
                InputArgs.MODE_DELETE,

                InputArgs.MODE_EXECUTE,
                InputArgs.MODE_EXPLAIN,

                InputArgs.MODE_FIND_WORDS,
                InputArgs.MODE_FIND_COMMANDS,

                InputArgs.MODE_SYNONYM_ADD,
                InputArgs.MODE_SYNONYM_DELETE,

                InputArgs.MODE_CONTEXT_CHANGE,

                InputArgs.MODE_HISTORY,
                InputArgs.MODE_STATISTIC
            ),
            help=
            '''
"q  COMMAND"        execute command
"qx COMMAND"        explain command

"qa COMMAND"        add command. IMPORTANT WORDS ORDER
"qe COMMAND"        edit command
"qd COMMAND"        delete command

"qfw"               print list of all words
"qfw WORD"          print list of all words that have link to WORD

"qfc"               print list of all commands
"qfc WORDS"         print list of all commands that have all WORDS

"qsa SYNONYM WORD"  add synonym
"qsd SYNONYM"       delete synonym

"qh"                print history
"qstat"             print statistic
            '''
        )
        parser.add_argument('command')

        try:
            args = parser.parse_args()
            return InputArgs(args.mode, args.command.split())
        except ArgumentParserException:
            return None

    def __print_history(self):
        for elem in self.db.get_history(20):
            print(elem)

    def __print_statistic(self):
        for elem in self.db.get_statistic(20):
            print(elem)
