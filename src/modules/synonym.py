from src.modules.module import Module


class SynonymModule(Module):
    def __init__(self, db, args):
        super().__init__(db)
        self.args = args

    def exec(self):
        if self.args.is_mode_synonym_add():
            self.__add_synonym()
        elif self.args.is_mode_synonym_delete():
            self.__delete_synonym()

        self.exit()

    def __add_synonym(self):
        if len(self.args.words) == 2:
            word = self.args.words[1]
            synonym = self.args.words[0]
            try:
                self.db.add_synonym(word, synonym)
            except KeyError:
                print('Word "{}" not found'.format(word))
            except ValueError:
                print('Synonym "{}" already exists'.format(synonym))
        else:
            print('Error: Expected "SYNONYM WORD" syntax')

    def __delete_synonym(self):
        if len(self.args.words) == 1:
            synonym = self.args.words[0]
            try:
                self.db.remove_synonym(synonym)
            except ValueError:
                print('Synonym "{}" does not exist'.format(synonym))
        else:
            print('Error: Expected "SYNONYM" syntax')
