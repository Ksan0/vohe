from src.modules.find_commands import FindCommandsModule
from src.modules.find_words import FindWordsModule
from src.modules.link import LinkModule
from src.modules.module import Module


class WordsModule(Module):
    def __init__(self, db, args):
        super().__init__(db)
        self.args = args

    def exec(self):
        words = [self.db.find_word(w) for w in self.args.words]

        if self.args.is_mode_add():
            self.add_words(words)
        elif self.args.is_mode_find_words():
            return FindWordsModule(self.db, self.args, words)
        elif self.args.is_mode_find_commands():
            return FindCommandsModule(self.db, self.args, words)

        return LinkModule(self.db, self.args, words)

    def add_words(self, words):
        self.db.add_primary_words([w for w in words if not w])
