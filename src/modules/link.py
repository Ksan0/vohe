import itertools

from src.modules.command import CommandModule
from src.modules.module import Module


class LinkModule(Module):
    def __init__(self, db, args, words):
        super().__init__(db)
        self.args = args
        self.words = words

    def exec(self):
        link, params = self.link()
        if link is None:
            print('Error: Cannot link "{}"'.format(' '.join([w.word for w in self.words])))
            self.exit()

        return CommandModule(self.db, self.args, link, params)

    def link(self):
        if self.args.is_mode_add():
            return self.words, []

        words = [w for w in self.words if w]
        params = [w for w in self.words if not w]

        def __is_link():
            for l, r in zip(left, right):
                if r.id not in l.links:
                    return False
            return True

        for link in itertools.permutations(words):
            left, right = itertools.tee(link)
            next(right, None)
            if __is_link() and self.db.has_command(link, params):
                return link, params

        return None, []
