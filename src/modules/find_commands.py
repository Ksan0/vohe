from src.modules.module import Module


class FindCommandsModule(Module):
    def __init__(self, db, args, words):
        super().__init__(db)
        self.args = args
        self.words = words

    def exec(self):
        if all(self.words):
            if self.words:
                commands = self.db.find_commands_with_words(self.words)
            else:
                commands = self.db.get_all_commands()

            for cmd in commands:
                print(' '.join([w.word for w in cmd.words]))

            if len(commands) == 0:
                print('Error: No commands found')
        else:
            print('Error: Has unknown words')
        self.exit()
