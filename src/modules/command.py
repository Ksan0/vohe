import itertools
import os

from src.conf.conf import Conf
from src.db.command import Command
from src.modules.delete import DeleteModule
from src.modules.edit import EditModule
from src.modules.execute import ExecuteModule
from src.modules.explain import ExplainModule
from src.modules.module import Module


class CommandModule(Module):
    def __init__(self, db, args, link, params):
        super().__init__(db)
        self.args = args
        self.link = link
        self.params = params

    def exec(self):
        if self.args.is_mode_add():
            self.add_command()
            self.exit()

        command = self.db.get_command(self.link, self.params)
        if not command:
            print('Error: Unknown command "{}"'.format(command))
            self.exit()

        script_path = self.get_script_path(command)

        if self.args.is_mode_execute():
            return ExecuteModule(self.db, command, script_path)
        elif self.args.is_mode_explain():
            return ExplainModule(self.db, command, script_path)
        elif self.args.is_mode_delete():
            return DeleteModule(self.db, command, script_path)
        elif self.args.is_mode_edit():
            return EditModule(self.db, command, script_path)
        else:
            print('Error: Unexpected execute mode {}'.format(command))
            self.exit()

    def add_command(self):
        command = Command(None, self.link, self.params)

        try:
            self.db.add_command(command)
        except KeyError:
            print('Error: Command {} already exists'.format(command))
            self.exit()

        script_path = self.get_script_path(command)
        template_path = self.get_template_path()
        if not os.path.isfile(script_path):
            with open(script_path, 'w') as script, open(template_path) as template:
                script.write(template.read())

    def get_script_path(self, command):
        return os.path.join(Conf.BASE_DIR, 'scripts', '{}.py'.format(command.id))

    def get_template_path(self):
        return os.path.join(Conf.BASE_DIR, 'res', 'template.py')
