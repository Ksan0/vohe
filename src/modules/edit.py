from src.db.context import Context
from src.modules.module import Module


class EditModule(Module):
    def __init__(self, db, command, script_path):
        super().__init__(db)
        self.command = command
        self.script_path = script_path

    def exec(self):
        context = Context(self)
        editor = context.get('_.self-terminal-editor')
        self.exit('{} {}'.format(editor, self.script_path))
