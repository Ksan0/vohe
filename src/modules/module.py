import os

from src.conf.conf import Conf


class Module(object):
    def __init__(self, db):
        self.db = db

    def exec(self):
        raise NotImplementedError()

    def exit(self, command=None):
        self.prepare_exit()
        extra_args = ('-c', '{}\n{}'.format(command, 'exec $SHELL')) if command is not None else ()
        os.execl(os.environ['SHELL'], os.environ['SHELL'], *extra_args)

    def prepare_exit(self):
        if self.db.is_open():
            path = self.db.get_context_path()
            with open(os.path.join(Conf.BASE_DIR, 'bash_ps1'), 'w') as file:
                file.write('PS1="[{}]$PS1"\n'.format(path))
                file.write('RCG_CTX="{}"\n'.format(path))
            self.db.close()
