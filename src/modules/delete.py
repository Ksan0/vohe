from src.modules.module import Module


class DeleteModule(Module):
    def __init__(self, db, command, script_path):
        super().__init__(db)
        self.command = command
        self.script_path = script_path

    def exec(self):
        self.db.remove_command(self.command)
        self.exit()
