import os
import traceback
import time

from src.conf.conf import Conf
from src.modules.input import InputModule
from src.db.db import DB


def main_unsafe():
    db = DB(Conf.DB_PATH)

    module = InputModule(db)

    while module is not None:
        # module_type = type(module)
        # t = time.time()
        module = module.exec()
        # t = time.time() - t
        # print('module %s exec in %.4f sec' % (module_type, t))


def main():
    try:
        main_unsafe()
    except BaseException as e:
        traceback.print_exception(type(e), e, None)
        os.execl(os.environ['SHELL'], os.environ['SHELL'])
