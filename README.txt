=== Installation ===

1. Download source
2. Add to .bashrc following lines

export RCG_HOME=~/work/projects/rcg
if [ -f $RCG_HOME/bash_ps1 ]; then
    . $RCG_HOME/bash_ps1
fi

function q  { exec python3 $RCG_HOME/run.py --mode execute "$*"; }
function qa { exec python3 $RCG_HOME/run.py --mode add "$*"; }
function qe { exec python3 $RCG_HOME/run.py --mode edit "$*"; }
function qd { exec python3 $RCG_HOME/run.py --mode delete "$*"; }
function qex { exec python3 $RCG_HOME/run.py --mode explain "$*"; }

function qfw { exec python3 $RCG_HOME/run.py --mode find-words "$*"; }
function qfc { exec python3 $RCG_HOME/run.py --mode find-commands "$*"; }

function qsa { exec python3 $RCG_HOME/run.py --mode synonym-add "$*"; }
function qsd { exec python3 $RCG_HOME/run.py --mode synonym-delete "$*"; }

function qx { exec python3 $RCG_HOME/run.py --mode context-change "$*"; }

function qh { exec python3 $RCG_HOME/run.py --mode history "$*"; }
function qstat { exec python3 $RCG_HOME/run.py --mode statistic "$*"; }


=== Usage ===

"q  COMMAND"        execute command
"qx COMMAND"        explain command

"qa COMMAND"        add command. IMPORTANT WORDS ORDER
"qe COMMAND"        edit command
"qd COMMAND"        delete command

"qfw"               print list of all words
"qfw WORD"          print list of all words that have link to WORD

"qfc"               print list of all commands
"qfc WORDS"         print list of all commands that have all WORDS

"qsa SYNONYM WORD"  add synonym
"qsd SYNONYM"       delete synonym

"qh"                print history
"qstat"             print statistic


q  : input -> words -> link -> command -> execute
qex: input -> words -> link -> command -> explain
qa : input -> words & new words -> link -> new command
qe : input -> words -> link -> command -> edit
qd : input -> words -> link -> command -> delete

qfw: input -> words -> link -> find & print words
qfc: input -> words -> link -> find & print command

qsa: input -> synonym
qsd: input -> synonym

qh: input
qstat: input


=== Database ===

words: id, word
synonyms: word_id, synonym
words_links: left_word, right_word
commands: id, words, path
words_to_commands: word_id, command_id
context: key, value (!)
statistic: mode, date, counter
history: mode, date, input


import os, subprocess
subprocess.Popen(executable='google-chrome', args=['google-chrome', 'ya.ru'], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
os.execl(os.environ['SHELL'], os.environ['SHELL'])


- пицца - сайт с пиццей
- суши - сайт с суши
- кино - сайт с кино
- погода - сайт с погодой
